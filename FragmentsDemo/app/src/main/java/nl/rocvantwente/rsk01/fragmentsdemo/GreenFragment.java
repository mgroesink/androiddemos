package nl.rocvantwente.rsk01.fragmentsdemo;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class GreenFragment extends Fragment {


    public GreenFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_green, container, false);
        v.setBackgroundColor(Color.GREEN);
        TextView tv = (TextView)v.findViewById(R.id.textviewGreen);
        tv.setText("Hallo allemaal");
        tv.setTextSize(48);
        return v;
    }

}
