package nl.rocvantwente.rsk01.fragmentsdemo;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    FragmentManager fm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fm = getSupportFragmentManager();
        Button btnRed = (Button)findViewById(R.id.buttonRed);
        Button btnBlue = (Button)findViewById(R.id.buttonBlue);
        Button btnGreen = (Button)findViewById(R.id.buttonGreen);
        btnRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = fm.beginTransaction();
                ft.add(R.id.fragment2 , new RedFragment());
                ft.commit();
            }
        });
        btnBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = fm.beginTransaction();
                ft.add(R.id.fragment2 , new BlueFragment());
                ft.commit();
            }
        });
        btnGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = fm.beginTransaction();
                ft.add(R.id.fragment2 , new GreenFragment());
                ft.commit();
            }
        });
    }
}
