package nl.rocvantwente.rsk01.scores;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private int teller = 0;

    private Button btnMin;
    private TextView tvCounter;
    private SharedPreferences mPrefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPrefs = getPreferences(MODE_PRIVATE);
        String name = "Jan";
        tvCounter = (TextView)findViewById(R.id.textViewCounter);
        btnMin =  (Button)findViewById(R.id.buttonMin);
        btnMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                teller--;
                tvCounter.setText("" + teller);
            }
        });
        Log.i("DEMO-I7AO" , "Application created");
    }

    public void IncreaseCounter(View view)
    {
        teller++;
        tvCounter.setText("" + teller);
    }

    //@Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        outState.putInt("teller" , teller);
//        Log.i("DEMO-I7AO" , "Application State Saved");
//    }
//
//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//        Log.i("DEMO-I7AO" , "Application State Restored");
//        teller = savedInstanceState.getInt("teller" , 0);
//        tvCounter.setText("" + teller);
//    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("DEMO-I7AO" , "Application stopped.");
        mPrefs.edit().putInt("teller" , teller).commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("DEMO-I7AO" , "Application started.");
    }

    @Override
    protected void onResume() {
        super.onResume();
        teller = mPrefs.getInt("teller" ,0);
        tvCounter.setText(""+ teller);
        Log.i("DEMO-I7AO" , "Application resumed.");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("DEMO-I7AO" , "Application destroyed.");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("DEMO-I7AO" , "Application paused.");
    }
}
