package nl.rocvantwente.rsk01.top2000i7aogroep2;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<Song> songs = new ArrayList<Song>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String json = loadJSONFromAsset(this);
        JSONArray obj;
        try {
           obj  = new JSONArray(json);

            for(int i = 0 ; i < obj.length();i++){
                Song s = new Song();
                JSONObject jobj = obj.getJSONObject(i);
                s.setTop2000Year(jobj.getInt("Top2000Year"));
                s.setPositie(jobj.getInt("Positie"));
                s.setTitel(jobj.getString("Titel"));
                s.setArtiest(jobj.getString("Artiest"));
                s.setJaar(jobj.getInt("Jaar"));
                songs.add(s);
            }
            ListView lvSongs = (ListView)findViewById(R.id.listViewSongs);
            SongAdapter adapter = new SongAdapter(this,0,songs);
            lvSongs.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String loadJSONFromAsset(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("top2000-2018.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        }
        catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }
}
