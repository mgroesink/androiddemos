package nl.rocvantwente.rsk01.top2000i7aogroep2;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class SongAdapter extends ArrayAdapter<Song> {


    public SongAdapter(Context context, int resource, List<Song> objects) {
        super(context, 0, objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Song s = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater
                    .from(getContext())
                    .inflate(R.layout.song_layout, parent, false);
        }
        TextView tvPosition = (TextView)convertView.findViewById(R.id.textViewPosition);
        TextView tvTitle = (TextView)convertView.findViewById(R.id.textViewTitle);
        TextView tvArtist = (TextView)convertView.findViewById(R.id.textViewArtist);
        TextView tvYear = (TextView)convertView.findViewById(R.id.textViewYear);

        tvPosition.setText("Positie: " + s.getPositie());
        tvArtist.setText(s.getArtiest());
        tvTitle.setText(s.getTitel());
        tvYear.setText("" + s.getJaar());
        if(position % 2 == 0){
            convertView.setBackgroundColor(Color.rgb(255,255,224));
        } else {
            convertView.setBackgroundColor(Color.rgb(224,224,255));

        }

        return convertView;
    }
}
