package nl.rocvantwente.rsk01.top2000i7aogroep2;

public class Song {
    private int Top2000Year;
    private int Positie;
    private String Titel;
    private String Artiest;
    private int Jaar;



    public Song() {
    }

    public int getTop2000Year() {
        return Top2000Year;
    }

    public void setTop2000Year(int top2000Year) {
        Top2000Year = top2000Year;
    }

    public int getPositie() {
        return Positie;
    }

    public void setPositie(int positie) {
        Positie = positie;
    }

    public String getTitel() {
        return Titel;
    }

    public void setTitel(String titel) {
        Titel = titel;
    }

    public String getArtiest() {
        return Artiest;
    }

    public void setArtiest(String artiest) {
        Artiest = artiest;
    }

    public int getJaar() {
        return Jaar;
    }

    public void setJaar(int jaar) {
        Jaar = jaar;
    }

    public Song(String titel, String artiest) {
        Titel = titel;
        Artiest = artiest;
    }

    public Song(int top2000Year, int positie, String titel, String artiest, int jaar) {
        Top2000Year = top2000Year;
        Positie = positie;
        Titel = titel;
        Artiest = artiest;
        Jaar = jaar;
    }
}
