package nl.rocvantwente.rsk01.morebuttons;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btnA,btnB,btnC,btnD;
    View.OnClickListener cl = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Button b = (Button)view;
            Toast.makeText(view.getContext(),
                    "You pressed button " + b.getText(),Toast.LENGTH_LONG).show();

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnA = (Button)findViewById(R.id.buttonA);
        btnB = (Button)findViewById(R.id.buttonB);
        btnC = (Button)findViewById(R.id.buttonC);
        btnD = (Button)findViewById(R.id.buttonD);
        btnB.setOnClickListener(cl);
        btnC.setOnClickListener(cl);
        btnD.setOnClickListener(cl);
        btnA.setOnClickListener(cl);
    }
}
