package nl.rocvantwente.rsk01.makephonecall;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button btnRed;
    Button btnGreen;
    RadioGroup rgColors;
    Button btnRoc;
    Button btnPhonecall;
    Button btnSms;
    Button btnEmail;
    Button btnOpen;
    int color = Color.RED;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        attachViews();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("HALLO" , "Mainactivity gestopt");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("HALLO" , "Mainactivity gekilled");
    }

    private void attachViews(){
        btnRed = (Button)findViewById(R.id.buttonOpenRed);
        btnGreen = (Button)findViewById(R.id.buttonOpenGreen);
        btnOpen = (Button)findViewById(R.id.buttonOpen);
        btnOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext() , ColoredActivity.class);
                i.putExtra("color" , color);
                startActivity(i);
            }
        });
        rgColors = (RadioGroup)findViewById(R.id.radiogroupColors);
        rgColors.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                Log.i("RSK01" , ""+i);
                switch(i){
                    case R.id.radioButtonRed:
                        color = Color.RED;
                        break;
                    case R.id.radioButtonOrange:
                        color = Color.rgb(255,165,0);
                        break;
                    case R.id.radioButtonGreen:
                        color = Color.GREEN;
                        break;
                }
            }
        });

        btnPhonecall = (Button)findViewById(R.id.buttonCall);
        btnSms = (Button)findViewById(R.id.buttonSMS);
        btnSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("smsto:0624950029");
                Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
                intent.putExtra("sms_body", "The SMS text");
                startActivity(intent);
            }
        });
        btnEmail = (Button)findViewById(R.id.buttonEmail);
        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/html");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "mroesink@rocvantwente.nl" });
                intent.putExtra(Intent.EXTRA_SUBJECT, "Dit is het onderwerp");
                intent.putExtra(Intent.EXTRA_TEXT, "Dit is de body");

                startActivity(Intent.createChooser(intent, "Send Email"));
            }
        });
        btnRoc = (Button)findViewById(R.id.buttonROC);

        btnRoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://www.rocvantwente.nl"));
                startActivity(i);
            }
        });

        btnRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =
                        new Intent(getApplicationContext(),
                                ColoredActivity.class);
                intent.putExtra("color" , Color.RED);
                startActivity(intent);

            }
        });

        btnGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btnPhonecall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_CALL,Uri.parse("tel:0624950029"));
                startActivity(i);
            }
        });


    }
}
