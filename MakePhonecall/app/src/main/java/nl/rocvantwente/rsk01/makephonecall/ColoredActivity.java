package nl.rocvantwente.rsk01.makephonecall;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

public class ColoredActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colored);
        Intent i = getIntent();
        int backgroundColor = i.getIntExtra("color" , 0);
        ConstraintLayout cl = (ConstraintLayout) findViewById(R.id.mainLayout);
        cl.setBackgroundColor(backgroundColor);
    }
}
