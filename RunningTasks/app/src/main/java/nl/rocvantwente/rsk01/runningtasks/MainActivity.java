package nl.rocvantwente.rsk01.runningtasks;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button buttonRun , buttonRunAsync , buttonCount;
    TextView tvCounter;
    ProgressBar pbSleep;

    class CountTo10 extends AsyncTask<Integer , Void , Void> {

        @Override
        protected Void doInBackground(Integer... integers) {
            try {
                Thread.sleep(5000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);


        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(getApplicationContext(),"Finished in background",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pbSleep = (ProgressBar)findViewById(R.id.progressBar) ;
        tvCounter = (TextView)findViewById(R.id.textViewCounter);
        buttonCount = (Button)findViewById(R.id.button3);
        buttonRun = (Button)findViewById(R.id.button);
        buttonRunAsync = (Button)findViewById(R.id.button2);
        buttonCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int counter = Integer.parseInt(tvCounter.getText().toString());
                tvCounter.setText("" + ++counter);
            }
        });
        buttonRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Toast.makeText(getApplicationContext(),
                        "Ready",Toast.LENGTH_SHORT).show();
            }
        });

        buttonRunAsync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CountTo10 task = new CountTo10();
                task.execute();

            }
        });


    }
}
